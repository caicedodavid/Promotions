from dataclasses import dataclass
from typing import Tuple, List

from flask import jsonify, render_template, request, current_app
from flask.views import MethodView
from flask_injector import inject
from flask_mongoengine.pagination import Pagination

from project.models import ProductsCollection, Products
from project.utils import is_palindrome, format_price


class HealthAPI(MethodView):
    def get(self) -> Tuple:
        return jsonify({'response': 'ok'}), 200


@inject
@dataclass
class SearchAPI(MethodView):
    products_collection: ProductsCollection

    def __paginate(self, products: List[Products], current_page: int):
        return Pagination(
            iterable=products,
            page=current_page,
            per_page=current_app.config.get('DOCUMENTS_PER_PAGE')
        )

    def get(self) -> Tuple:
        query = request.args.get('query', '')
        current_page = int(request.args.get('page', 1))
        products = self.products_collection.get_all(query)
        has_offer = is_palindrome(query) if query else False
        products = self.__paginate(products, current_page)
        return render_template(
            'search.html',
            products=products,
            has_offer=has_offer,
            format_price=format_price,
            discount_percentage=current_app.config.get('DISCOUNT_PERCENTAGE')
        )
