import os


class BaseConfig:
    """ Base configuration """
    TESTING = False
    DEBUG = True
    MONGODB_HOST = os.environ.get('MONGODB_HOST')
    DOCUMENTS_PER_PAGE = int(os.environ.get('DOCUMENTS_PER_PAGE'))
    DISCOUNT_PERCENTAGE = float(os.environ.get('DISCOUNT_PERCENTAGE'))
    DEFAULT_LOCALE = os.environ.get('DEFAULT_LOCALE')


class DevelopmentConfig(BaseConfig):
    """ Development configuration """
    PRESERVE_CONTEXT_ON_EXCEPTION = False


class TestingConfig(BaseConfig):
    """ Testing configuration """
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    MONGODB_HOST = 'mongomock://localhost'


class ProductionConfig(BaseConfig):
    """ Production configuration """
    DEBUG = False
