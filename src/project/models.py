from typing import List
from functools import lru_cache
from project import mongo
from mongoengine.queryset import QuerySet
from mongoengine.queryset.visitor import Q


class Products(mongo.Document):
    meta = {'collection': 'products'}

    product_id = mongo.ObjectIdField(primary_key=True)
    stored_id = mongo.IntField(db_field='id', unique=True, null=False)
    brand = mongo.StringField(required=True, null=False)
    description = mongo.StringField(required=True, null=False)
    image = mongo.StringField(required=True, null=False)
    price = mongo.IntField(required=True, null=False)


class ProductsCollection:
    def __init__(self):
        self.products_model = Products

    def get_by_id(self, id: int) -> QuerySet:
        return self.products_model.objects(stored_id=id).first()

    def get_by_string_in_brand_or_name(self, query: str) -> QuerySet:
        description_contains_query = Q(description__contains=query)
        brand_contains_query = Q(brand__contains=query)
        return self.products_model.objects(
            description_contains_query | brand_contains_query
        )

    @lru_cache
    def get_all(self, query: str) -> List[Products]:
        if query.isdigit():
            return [self.get_by_id(id=int(query))]

        return list(self.get_by_string_in_brand_or_name(query))
