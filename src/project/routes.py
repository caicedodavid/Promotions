from project.endpoints import HealthAPI, SearchAPI


def register(app):
    app.add_url_rule(
        "/health",
        view_func=HealthAPI.as_view("health"))

    app.add_url_rule(
        "/search",
        view_func=SearchAPI.as_view("search"))
