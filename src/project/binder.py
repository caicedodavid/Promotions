from flask_injector import singleton
from injector import Binder

from project.models import ProductsCollection


def configure(binder: Binder):
    bind_interfaces(binder, {
        ProductsCollection: ProductsCollection,
    })


def bind_interfaces(binder: Binder, interfaces: dict):
    for interface, implementation in interfaces.items():
        binder.bind(
            interface,
            to=implementation,
            scope=singleton
        )
