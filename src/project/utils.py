from babel.numbers import format_decimal

from flask import current_app


def is_palindrome(string: str):
    return string == string[::-1]


def format_price(price: float, discount_percentage: float = 0):
    final_price = price * (1 - discount_percentage)
    return '${}'.format(format_decimal(
        final_price,
        format='#,##0.##',
        locale=current_app.config.get('DEFAULT_LOCALE')
    ))
