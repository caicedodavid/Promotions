from mongomock.object_id import ObjectId
products = [
    {
        "product_id": bytes(str(ObjectId())[:12], 'ascii'),
        "stored_id": 2,
        "brand": "SuperMarca",
        "description": "Tabbavisión",
        "image": "foo.png",
        "price": 500000
    },
    {
        "product_id": bytes(str(ObjectId())[:12], 'ascii'),
        "stored_id": 3,
        "brand": "labba",
        "description": "Televisión 8k",
        "image": "foo.png",
        "price": 540000
    },
    {
        "product_id": bytes(str(ObjectId())[:12], 'ascii'),
        "stored_id": 181,
        "brand": "SuperMarca",
        "description": "Televisión 8k",
        "image": "foo.png",
        "price": 540000
    },
    {
        "product_id": bytes(str(ObjectId())[:12], 'ascii'),
        "stored_id": 182,
        "brand": "sebbe",
        "description": "tebbe",
        "image": "foo.png",
        "price": 540000
    },
    {
        "product_id": bytes(str(ObjectId())[:12], 'ascii'),
        "stored_id": 1,
        "brand": "SuperMarca2",
        "description": "Labbadora",
        "image": "foo.png",
        "price": 690000
    }
]
