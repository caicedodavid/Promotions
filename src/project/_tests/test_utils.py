from math import pi
from unittest import TestCase

from project.utils import is_palindrome, format_price
from .utils import random_string


class TestUtils(TestCase):
    def test_is_palindrome_even_size(self):
        string = random_string(1000)
        inverted_string = string[::-1]
        palindrome = string + inverted_string
        self.assertTrue(is_palindrome(palindrome))
        not_palindrome = string + '1' + inverted_string[1:]
        self.assertFalse(is_palindrome(not_palindrome))

    def test_is_palindrome_uneven_size(self):
        string = random_string(1000)
        inverted_string = string[::-1]
        palindrome = string + inverted_string[1:]
        self.assertTrue(is_palindrome(palindrome))
        palindrome = string[:-1] + '1' + inverted_string[1:]
        self.assertTrue(is_palindrome(palindrome))
        not_palindrome = string[:-1] + '12' + inverted_string[2:]
        self.assertFalse(is_palindrome(not_palindrome))

    def test_format_price_no_discount_percentage(self):
        self.assertEquals(format_price(42), '$42')
        self.assertEquals(format_price(1000), '$1.000')
        self.assertEquals(format_price(10000), '$10.000')
        self.assertEquals(format_price(100000), '$100.000')
        self.assertEquals(format_price(1000000), '$1.000.000')

    def test_format_price_with_discount_percentage(self):
        self.assertEquals(format_price(pi, 0), '$3,14')
        self.assertEquals(format_price(42, 0.5), '$21')
        self.assertEquals(format_price(1001, 0.25), '$750,75')
        self.assertEquals(format_price(10001, 0.5), '$5.000,5')
