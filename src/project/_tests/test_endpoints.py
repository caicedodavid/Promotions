from unittest.mock import Mock
from flask_injector import request

from project._tests.base import BaseTestCase, ProductsCollectionMixin
from project.models import ProductsCollection


class TestEndpoints(BaseTestCase, ProductsCollectionMixin):
    def configure_injector(self, binder):
        self.products_collection_mock = Mock()
        binder.bind(
            ProductsCollection,
            to=self.products_collection_mock,
            scope=request,
        )

    def test_health(self) -> None:
        with self.client:
            response = self.client.get('/health')
            self.assertStatus(response, 200)

    def test_search_ok(self) -> None:
        self.products_collection_mock.get_all.return_value = []
        with self.client:
            response = self.client.get('/search')
            self.assertStatus(response, 200)
            self.assert_template_used('search.html')
            content = response.data.decode('utf-8')
            self.assertIn(
                'No encontramos resultados para tu búsqueda',
                content
            )
