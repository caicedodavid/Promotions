from flask_injector import request

from project._tests.base import BaseTestCase, ProductsCollectionMixin
from project.models import ProductsCollection


class TestEndpointsIntegrateModels(BaseTestCase, ProductsCollectionMixin):
    def configure_injector(self, binder):
        binder.bind(
            ProductsCollection,
            to=ProductsCollection,
            scope=request,
        )

    def setUp(self):
        self.load_products()

    def test_search_abba(self) -> None:
        with self.client:
            response = self.client.get('/search?query=abba')
            content = response.data.decode('utf-8')
            self.assertEquals(content.count('product-block flex-column'), 3)
            self.assertIn('Tabbavisión', content)
            self.assertIn('labba', content)
            self.assertIn('Labbadora', content)
            self.assertIn('walmart-discount-percentage-card', content)

    def test_search_181(self) -> None:
        with self.client:
            response = self.client.get('/search?query=181')
            content = response.data.decode('utf-8')
            self.assertEquals(content.count('product-block flex-column'), 1)
            self.assertIn('foo.png', content)
            self.assertIn('walmart-discount-percentage-card', content)

    def test_search_no_offer(self) -> None:
        with self.client:
            response = self.client.get('/search?query=sebbe')
            content = response.data.decode('utf-8')
            self.assertEquals(content.count('product-block flex-column'), 1)
            self.assertNotIn('walmart-discount-percentage-card', content)
