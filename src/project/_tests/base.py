from abc import ABC

from flask import current_app as app
from flask_injector import FlaskInjector
from flask_testing import TestCase
from mongoengine import connect, disconnect

from .fixtures.products import products as products_data
from project.models import ProductsCollection


class BaseTestCase(TestCase):
    def configure_injector(self, binder):
        pass

    def create_app(self) -> None:
        disconnect('default')
        app.config.from_object('project.configs.TestingConfig')
        FlaskInjector(
            app=app,
            modules=[self.configure_injector],
            injector=app.config.get('injector')
        )
        connect('test', host='mongomock://localhost')
        return app


class ProductsCollectionMixin(ABC):
    def load_products(self):
        self.products_collection = ProductsCollection()
        products = []
        for product in products_data:
            products.append(self.products_collection.products_model(**product))
        self.products_collection.products_model.objects.insert(products)
