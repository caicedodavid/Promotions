from .base import BaseTestCase, ProductsCollectionMixin


class TestProductsCollection(BaseTestCase, ProductsCollectionMixin):
    def setUp(self):
        self.load_products()

    def test_get_by_id(self):
        product = self.products_collection.get_by_id(1)
        self.assertEquals(product.stored_id, 1)

    def test_get_by_string_in_brand_or_name(self):
        query_text = 'abba'
        products = self.products_collection.\
            get_by_string_in_brand_or_name(query_text)
        self.assertEquals(len(products), 3)
        for product in products:
            self.assertTrue(
                query_text in product.description
                or query_text in product.brand
            )

    def test_get_all_text_string(self):
        query_text = 'Labbadora'
        products = self.products_collection.get_all(query=query_text)
        self.assertEquals(len(products), 1)
        self.assertEquals(products[0].stored_id, 1)

    def test_get_all_digit_string(self):
        query_text = '181'
        products = self.products_collection.get_all(query=query_text)
        self.assertEquals(len(products), 1)
        self.assertEquals(products[0].stored_id, 181)

    def test_get_all_no_dups(self):
        query_text = 'ebbe'
        products = self.products_collection.get_all(query=query_text)
        self.assertEquals(len(products), 1)
        self.assertEquals(products[0].stored_id, 182)

    def test_get_all_cache(self):
        query_text = 'ebbe'
        self.products_collection.get_all(query=query_text)
        initial_hits = self.products_collection.get_all.cache_info().hits
        self.products_collection.get_all(query=query_text)
        final_hits = self.products_collection.get_all.cache_info().hits
        self.assertGreater(final_hits, initial_hits)
