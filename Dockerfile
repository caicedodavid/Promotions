FROM python:3.9.0-alpine3.12

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PORT 8000
ENV FLASK_APP /app/src/app.py
ENV PYTHONUNBUFFERED 1
ENV APP_CONFIG project.configs.ProductionConfig
ENV MONGODB_HOST mongodb+srv://productListUser:productListPassword@cluster0.ktiri.mongodb.net/promotions?retryWrites=true&w=majority
ENV DISCOUNT_PERCENTAGE 0.5
ENV DOCUMENTS_PER_PAGE 12
ENV DEFAULT_LOCALE es_CO


COPY requirements.txt /app/

RUN pip install -r /app/requirements.txt

WORKDIR /app/src

COPY . /app/

RUN chmod +x /app/entrypoint.sh

EXPOSE $PORT

CMD ["/app/entrypoint.sh"]