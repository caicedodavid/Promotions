coverageStdout=$(docker-compose exec -T web flask cov)
if (($? != 0))
then
    echo "$coverageStdout"
    echo Failed Tests
    exit 1
fi

echo "$coverageStdout"
coverageStdoutLastWord=$(echo "$coverageStdout" | tail -n 1 | awk '{print $NF}' | tr -d '\n' | tr -d '\r') 
coveragePercentage=${coverageStdoutLastWord%?}
if [ $coveragePercentage -lt 100 ]
then
    echo Please, work on your tests
    exit 1
else
    echo Attaboy
    exit 0
fi